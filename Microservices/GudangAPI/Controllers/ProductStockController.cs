﻿using GudangAPI.Entity;
using GudangAPI.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace GudangAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductStockController : ControllerBase
    {
        private readonly IProductStockServices _productStockServices;

        public ProductStockController(IProductStockServices productStockServices)
        {
            _productStockServices = productStockServices;
        }

        [HttpPost("AddStock")]
        public async Task AddStock(int productId, int count, string activity, string activityId, string requestId)
        {
            await _productStockServices.AddStock(productId, count, activity, activityId);
        }

        [HttpPost("ReduceStock")]
        public async Task ReduceStock(int productId, int count, string activity, string activityId, string requestId)
        {
            await _productStockServices.ReduceStock(productId, count, activity, activityId);
        }

        [HttpPost("Rollback")]
        public async Task Rollback(string activity, string activityId, string requestId)
        {
            await _productStockServices.Rollback(activity, activityId);
        }
    }
}
