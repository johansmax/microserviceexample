﻿using System.ComponentModel.DataAnnotations;

namespace GudangAPI.Entity
{
    public sealed class ProductStock
    {

        public int Id { get; set; }
        [Required]
        [MaxLength(50, ErrorMessage = "Max 50 chars")]
        public string Name { get; set; }
        [Required]

        [MaxLength(50, ErrorMessage = "Max 50 chars")]
        public string Category { get; set; }
        [Required]
        [Range(0, 100000000)]
        public int Stock { get; set; }

        

    }

    public sealed class Customer
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(50, ErrorMessage = "Max 50 chars")]
        public string Name { get; set; }
        [Required]
        [MaxLength(50, ErrorMessage = "Max 50 chars")]
        public string Email { get; set; }
        [Required]
        [MaxLength(50, ErrorMessage = "Max 50 chars")]
        public string Phone { get; set; }
        [Required]
        [MaxLength(200, ErrorMessage = "Max 200 chars")]
        public string Address { get; set; }
    }
    
}
