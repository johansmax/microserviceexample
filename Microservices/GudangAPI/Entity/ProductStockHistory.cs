﻿using System.ComponentModel.DataAnnotations;

namespace GudangAPI.Entity;

public class ProductStockHistory
{
    public int Id { get; set; }
    public int ProductStockId { get; set; }
    public ProductStock ProductStock { get; set; }
    public int Count { get; set; }
    public int Before { get; set; }
    public int After { get; set; }
    [MaxLength(50)]
    public string Activity { get; set; }


    [MaxLength(50)]
    public string ActivityId { get; set; }
    public DateTime CreatedAt { get; set; }
}