﻿using GudangAPI.Entity;
using Microsoft.EntityFrameworkCore;

namespace GudangAPI.Infrastructure;

public interface IProductStockRepository : IRepository<ProductStock>
{
    Task<List<ProductStock>> GetByProductName(string name);

    Task<ProductStock> FindById(int id);
}

public sealed class ProductStockRepository : Repository<ProductStock>, IProductStockRepository
{
    public ProductStockRepository(DataContext context) : base(context)
    {
    }

   

    public async Task<List<ProductStock>> GetByProductName(string name)
    {
        return await _context.ProductStocks.Where(x => x.Name.Contains(name) || x.Category.Contains(name)).ToListAsync();
    }
}


