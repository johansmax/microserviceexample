﻿using GudangAPI.Entity;

namespace GudangAPI.Infrastructure
{
    public interface IProductStockServices
    {
        Task<ProductStock> AddStock(int productId, int count, string activity, string activityId);
        Task<ProductStock> ReduceStock(int productId, int count, string activity, string activityId);
        Task Rollback(string activity, string activityId);
    }

    public class ProductStockServices : IProductStockServices
    {
        private readonly IProductStockRepository _productStockRepository;
        private readonly IProductStockHistoryRepository _historyRepository;

        public ProductStockServices(IProductStockRepository productStockRepository,
            IProductStockHistoryRepository historyRepository)
        {
            _productStockRepository = productStockRepository;
            _historyRepository = historyRepository;
        }

        public async Task<ProductStock> AddStock(int productId, int count, string activity, string activityId)
        {
            return await UpdateStock(productId, count, activity, activityId, true);
        }

        public async Task<ProductStock> ReduceStock(int productId, int count, string activity, string activityId)
        {
            return await UpdateStock(productId, count, activity, activityId, false);
        }

        public async Task Rollback(string activity, string activityId)
        {
            var list = await _historyRepository.FindByActivity(activity, activityId);
            foreach (var productStockHistory in list)
            {
                var temp = new ProductStockHistory()
                {
                    ProductStockId = productStockHistory.ProductStockId,
                    Count = -productStockHistory.Count,
                    Before = productStockHistory.ProductStock.Stock,
                    After = productStockHistory.ProductStock.Stock + -productStockHistory.Count,
                    Activity = activity + "[Rollback]",
                    ActivityId = activityId,
                    CreatedAt = DateTime.Now
                };

                _productStockRepository.Update(productStockHistory.ProductStock);
                _historyRepository.Add(temp);

                
            }
            _productStockRepository.Save();
        }

        private async Task<ProductStock> UpdateStock(int productId, int count, string activity, string activityId, bool isIncrement)
            {
                var productStock = await _productStockRepository.FindById(productId);
                if (productStock == null)
                {
                    productStock = new ProductStock
                    {
                        Id = productId,
                        Stock = count
                    };
                    _productStockRepository.Add(productStock);
                }
                else
                {
                    productStock.Stock += isIncrement ? count : -count;
                    _productStockRepository.Update(productStock);
                }

                var history = new ProductStockHistory
                {
                    ProductStockId = productStock.Id,
                    Count = (isIncrement ? count : -count),
                    Before = productStock.Stock - (isIncrement ? count : -count),
                    After = productStock.Stock,
                    Activity = activity,
                    ActivityId = activityId,
                    CreatedAt = DateTime.Now
                };
                _historyRepository.Add(history);
                _historyRepository.Save();
                return productStock;
            }
        }
    }
