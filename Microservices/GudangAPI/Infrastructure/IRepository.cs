﻿namespace GudangAPI.Infrastructure;

public interface IRepository<T> where T : class
{
    void Add(T entity);
    void Update(T entity);
    void Delete(T entity);
    void Save();

    Task<T> FindById(int id);
}