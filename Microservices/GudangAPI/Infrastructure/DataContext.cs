﻿using GudangAPI.Entity;
using Microsoft.EntityFrameworkCore;

namespace GudangAPI.Infrastructure
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }
        public DbSet<ProductStock> ProductStocks { get; set; }
        public DbSet<ProductStockHistory> ProductStockHistories { get; set; }
        public DbSet<Customer> Customers { get; set; }

    }
}
