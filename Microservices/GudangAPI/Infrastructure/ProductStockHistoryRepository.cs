﻿using GudangAPI.Entity;
using Microsoft.EntityFrameworkCore;

namespace GudangAPI.Infrastructure;

public class ProductStockHistoryRepository : Repository<ProductStockHistory>, IProductStockHistoryRepository
{
    public ProductStockHistoryRepository(DataContext context) : base(context)
    {
    }

    public async Task<List<ProductStockHistory>> FindByActivity(string activity, string activityId)
    {
        return await this._dbSet
            .Where(x => x.Activity == activity && x.ActivityId == activity)
            .Include(x=>x.ProductStock)
            .ToListAsync();

    }
}

public interface IProductStockHistoryRepository : IRepository<ProductStockHistory>
{
    Task<List<ProductStockHistory>> FindByActivity(string activity, string activityId);
}