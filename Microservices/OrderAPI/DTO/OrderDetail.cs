﻿using System.ComponentModel.DataAnnotations;

namespace OrderAPI.DTO;

public sealed class OrderDetail
{
    [Key]
    public int OrderDetailId { get; set; }

    [Required]
    public int OrderId { get; set; }

    public int Id { get; set; }
    [Required]
    [MaxLength(50, ErrorMessage = "Max 50 chars")]
    [RegularExpression(@"^[a-zA-Z0-9\s]+$", ErrorMessage = "only accept alpha numeric")]
    public string Name { get; set; }
    [Required]

    [MaxLength(50, ErrorMessage = "Max 50 chars")]
    public string Category { get; set; }
    [Required]
    [Range(0, 100000000, ErrorMessage = "min value 0 max value 10000000")]
    public int Count { get; set; }
    [Required]
    public decimal Price { get; set; }
}