﻿namespace OrderAPI.Infrastructure
{
    public interface IStockUpdater
    {
        void Reduce(int productId, int count, string orderNo, string activity, string requestId);
        void Rollback(string orderNo, string activity, string requestId);
    }

}
