﻿using OrderAPI.Entity;

namespace OrderAPI.Infrastructure;

public interface IOrderService
{
    Task<OrderHeader> CreateOrder(OrderHeader order, IEnumerable<OrderDetail> orderDetails, string requestId);
    Task<OrderHeader> GetOrderById(int id);
    Task<List<OrderHeader>> GetOrdersByCustomerName(string name);
}

public sealed class OrderService(IOrderRepository orderRepository, IOrderDetailRepository orderDetailRepository,
    IStockUpdater stockUpdater) : IOrderService
{
    public async Task<OrderHeader> CreateOrder(OrderHeader order, IEnumerable<OrderDetail> orderDetails,
        string requestId)
    {
        try
        {
            orderRepository.Add(order);
            foreach (var orderDetail in orderDetails)
            {
                orderDetailRepository.Add(orderDetail);
                stockUpdater.Reduce(orderDetail.Id, orderDetail.Count, order.OrderNo, "Order", requestId);

            }
            orderRepository.Save();
            return order;
        }
        catch (Exception e)
        {
            order.OrderStatus = "Failed";
            orderRepository.Update(order);
            stockUpdater.Rollback(order.OrderNo, "Order", requestId);

            throw;
        }

    }

    public async Task<OrderHeader> GetOrderById(int id)
    {
        return await orderRepository.FindById(id);
    }

    public async Task<List<OrderHeader>> GetOrdersByCustomerName(string name)
    {
        return await orderRepository.GetByCustomerName(name);
    }
}