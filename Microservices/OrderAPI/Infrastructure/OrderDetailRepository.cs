﻿using OrderAPI.Entity;

namespace OrderAPI.Infrastructure;

public interface IOrderDetailRepository : IRepository<OrderDetail>
{
}

public sealed class OrderDetailRepository : Repository<OrderDetail>,IOrderDetailRepository
{
    public OrderDetailRepository(DataContext context) : base(context)
    {
    }
}