﻿using System;
using System.Text;
using Azure.Core;
using Newtonsoft.Json;

namespace OrderAPI.Infrastructure;

public sealed class StockUpdater : IStockUpdater
{
    public StockUpdater()
    {
    }

  public async void Rollback( string orderNo, string activity, string requestId)
    {
        var url = "https://localhost:7244/api/ProductStock/RollBack"; 

        var httpClient = new HttpClient();
       
        var requestData = new
        {
            activity,
            activityId = orderNo,
            requestId
        };
        var json = JsonConvert.SerializeObject(requestData);

        var content = new StringContent(json, Encoding.UTF8, "application/json");

        HttpClient client = new HttpClient();
        client.DefaultRequestHeaders.Add("X-Api-Key", "1234");

        var response = await client.PostAsync(url, content);
        if (!response.IsSuccessStatusCode)
        {
            throw new InvalidOperationException("Failed to update stock.StatusCode" + response.StatusCode);
        }
    }

    public async void Reduce(int productId, int count, string orderNo, string activity, string requestId)
    {
        var url = "https://localhost:7244/api/ProductStock/ReduceStock";
        var requestData = new
        {
            productId,
            count,
            activity,
            activityId = orderNo,
            requestId
        };

        var json = JsonConvert.SerializeObject(requestData);
        var content = new StringContent(json, Encoding.UTF8, "application/json");
        HttpClient client = new HttpClient();
        client.DefaultRequestHeaders.Add("X-Api-Key", "1234");
        var response = await client.PostAsync(url, content);
        if (!response.IsSuccessStatusCode)
        {
            throw new InvalidOperationException("Failed to rollback stock.StatusCode:" + response.StatusCode);
        }
    }

}