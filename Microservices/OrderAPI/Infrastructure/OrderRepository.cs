﻿using Microsoft.EntityFrameworkCore;
using OrderAPI.Entity;

namespace OrderAPI.Infrastructure;

public interface IOrderRepository : IRepository<OrderHeader>
{
    Task<List<OrderHeader>> GetByCustomerName(string name);
}

public sealed class OrderRepository : Repository<OrderHeader>, IOrderRepository
{
    public OrderRepository(DataContext context) : base(context)
    {
    }

    public async Task<List<OrderHeader>> GetByCustomerName(string name)
    {
        return await _context.OrderHeaders.Where(x => x.CustomerName.Contains(name)).ToListAsync();
    }

    public override async Task<OrderHeader> FindById(int id)
    {
        return await _dbSet.
            Include(x => x.OrderDetails)
            .FirstOrDefaultAsync(x => x.OrderId == id);
    }
}

