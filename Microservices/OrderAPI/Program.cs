
using Microsoft.EntityFrameworkCore;
using OrderAPI.Infrastructure;

namespace OrderAPI
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            builder.Services.AddDbContext<DataContext>(options =>
                options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")));
            builder.Services.AddScoped(typeof(IOrderRepository), typeof(OrderRepository));
            builder.Services.AddScoped(typeof(IOrderDetailRepository), typeof(OrderDetailRepository));
            builder.Services.AddScoped(typeof(IStockUpdater), typeof(StockUpdater));
            builder.Services.AddScoped(typeof(IOrderService), typeof(OrderService));

            builder.Services.AddControllers();
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseHttpsRedirection();

            app.UseAuthorization();
           // app.UseMiddleware<ApiKeyMiddleware>();


            app.MapControllers();

            app.Run();

        }
    }
}
