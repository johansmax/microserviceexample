﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OrderAPI.Infrastructure;

namespace OrderAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IOrderService _orderService;

        public OrderController(IOrderService orderService)
        {
            _orderService = orderService;
        }

        [HttpPost]
        public async Task<IActionResult> CreateOrder(DTO.OrderHeader order)
        {
            var header = new Entity.OrderHeader()
            {
                OrderDate = order.OrderDate,
                OrderDetails = order.OrderDetails.Select(x => new Entity.OrderDetail
                {
                    Id = x.Id,
                    Price = x.Price,
                    Count = x.Count,
                    Name = x.Name,
                    Category = x.Category
                }).ToList()
            };
            var result = await _orderService.CreateOrder(header, header.OrderDetails, Guid.NewGuid().ToString());
            return Ok(result);
        }
    }
}
