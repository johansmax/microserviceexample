﻿using Azure.Core;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Text;
using TokoOnline.DTO;
using TokoOnline.Model;

namespace TokoOnline.Infrastructure
{
    public class OrderService
    {
        public OrderService()
        {
        }

        public async Task CreateOrder(ApplicationUser user, string sendAdrres, List<OrderDetail> details)
        {
            var header = new OrderHeader
            {
                Email = user.Email,
                CustomerName = user.UserName,
                OrderDate = DateTime.Now,
                SendAddress = sendAdrres,
                OrderStatus = "Pending",
                OrderNo = GetOrderNo(),
                OrderDetails = details
            };

            var url = "https://localhost:7244/api/ProductStock/RollBack";

            var httpClient = new HttpClient();

         
            var json = JsonConvert.SerializeObject(header);

            var content = new StringContent(json, Encoding.UTF8, "application/json");

            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("X-Api-Key", "1234");

            var response = await client.PostAsync(url, content);
            if (!response.IsSuccessStatusCode)
            {
                throw new InvalidOperationException("Failed to update stock.StatusCode" + response.StatusCode);
            }
        }

        private string GetOrderNo()
        {
            return DateTime.Now.ToString("OrderyyyyMMddHHmmss");
        }
    }
}
