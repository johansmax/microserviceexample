﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Security.Policy;
using System.Text;
using TokoOnline.DTO;
using TokoOnline.Model;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace TokoOnline.Controllers
{
    public class HomeController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;

        public HomeController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [Route("submit")]
        public async Task<IActionResult> Submit(IFormCollection form)
        {
           
                var userId = User.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier)?.Value;
                var userName = User.Identity.Name;
                var email = User.Identities.FirstOrDefault().Claims.ToList()[2].Value;
          

            var orderHeader = new OrderHeader
            {
                OrderDate = DateTime.Now,
                OrderNo = DateTime.Now.ToString("yyyyMMddhhmmss"),
                CustomerName = userName,
                Email = email,
                OrderStatus = "Pending",
                SendAddress = "hello world",
                OrderDetails = new List<OrderDetail>()
            };
         
            var itemIds = form["itemIds[]"];
            var itemNames = form["itemNames[]"];
            var itemPrices = form["itemPrices[]"];
            var quantities = form["quantities[]"];

            for (int i = 0; i < itemIds.Count; i++)
            {
                OrderDetail detail = new OrderDetail
                {
                    Id = int.Parse(itemIds[i]),
                    Name = itemNames[i],
                    Price = decimal.Parse(itemPrices[i]),
                    Count = int.Parse(quantities[i])
                };
           
                orderHeader.OrderDetails.Add(detail);
            }

            var client = new HttpClient();
            client.BaseAddress = new Uri("https://localhost:7187/");
            var json = JsonConvert.SerializeObject(orderHeader);

            var content = new StringContent(json, Encoding.UTF8, "application/json");

            var response = await client.PostAsync("api/Order", content);
            if (!response.IsSuccessStatusCode)
            {
                throw new InvalidOperationException("Barang gagal dipesan. ErrorCode:" + response.StatusCode);
            }

          

            return Ok(new { message = "Order berhasil dibuat"});
        }



    }
}
