﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace TokoOnline.Controllers
{
    public class SearchController : Controller
    {
        public SearchController()
        {
        }

        [HttpGet]
        public async Task<IActionResult> Index(string query)
        {
            try
            {
                var client = new HttpClient();

                client.BaseAddress = new Uri("https://localhost:7265/");

                var response = await client.GetAsync($"api/Product?requestId=2342342&name={query}");
                if (response.IsSuccessStatusCode)
                {
                    var temp = await response.Content.ReadAsStringAsync();
                    var products = JsonConvert.DeserializeObject<List<Item>>(temp);
                    return Json(products);
                }
                else
                {
                    return StatusCode((int)response.StatusCode, response.ReasonPhrase);
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

       
    }

    public class Item
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
    }
}
