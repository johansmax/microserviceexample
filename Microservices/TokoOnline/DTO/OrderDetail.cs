﻿using System.ComponentModel.DataAnnotations;

namespace TokoOnline.DTO;

public sealed class OrderDetail
{
    [Key]
    public int OrderDetailId { get; set; }

    [Required]
    public int OrderId { get; set; }

    public int Id { get; set; }
    [Required]
    [MaxLength(50, ErrorMessage = "Max 50 chars")]
    public string Name { get; set; }
    [Required]

    [MaxLength(50, ErrorMessage = "Max 50 chars")]
    public string Category { get; set; }
    [Required]
    [Range(0, 100000000)]
    public int Count { get; set; }
    [Required]
    public decimal Price { get; set; }
}