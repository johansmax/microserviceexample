﻿namespace TokoOnline.DTO;

public class CartItem
{
    public int ItemId { get; set; }
    public string ItemName { get; set; }
    public decimal ItemPrice { get; set; }
    public int Quantity { get; set; }
}