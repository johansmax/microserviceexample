﻿using System.ComponentModel.DataAnnotations;

namespace TokoOnline.DTO
{
    public sealed class OrderHeader
    {
        public int OrderId { get; set; }
        public string OrderNo { get; set; }
        [Required]
        [MaxLength(50)]
        public string Email { get; set; }
        [Required]
        [MaxLength(50)]
        public string CustomerName { get; set; }

        [Required]
        [MaxLength(200, ErrorMessage = "Max chars 200")]
        public string SendAddress { get; set; }

        [Required] public DateTime OrderDate { get; set; }

        [Required][MaxLength(50)] public string OrderStatus { get; set; }

        public ICollection<OrderDetail> OrderDetails { get; set; }
    }
}
