﻿using Microsoft.AspNetCore.Identity;

namespace TokoOnline.Model;

public class ApplicationUser : IdentityUser
{
}