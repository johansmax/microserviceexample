﻿using System.ComponentModel.DataAnnotations;

namespace TokoOnline.Model
{
    public class RegisterViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [MinLength(3, ErrorMessage = "Minimum 3 chars")]
        [MaxLength(50, ErrorMessage = "Maximum 50 chars")]
        public string Name { get; set; }
        [Required]
        [DataType(DataType.Password)]
        [MinLength(4,ErrorMessage = "Minimum 4 chars")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [MinLength(4, ErrorMessage = "Minimum 4 chars")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

}
