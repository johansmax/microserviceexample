USE [ProductDB]
GO
SET IDENTITY_INSERT [dbo].[Product] ON 
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (2, N'HL Road Frame - Black, 58', N'Components-Road Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (3, N'HL Road Frame - Red, 58', N'Components-Road Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (4, N'Sport-100 Helmet, Red', N'Accessories-Helmets', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (5, N'Sport-100 Helmet, Black', N'Accessories-Helmets', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (6, N'Mountain Bike Socks, M', N'Clothing-Socks', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (7, N'Mountain Bike Socks, L', N'Clothing-Socks', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (8, N'Sport-100 Helmet, Blue', N'Accessories-Helmets', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (9, N'AWC Logo Cap', N'Clothing-Caps', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (10, N'Long-Sleeve Logo Jersey, S', N'Clothing-Jerseys', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (11, N'Long-Sleeve Logo Jersey, M', N'Clothing-Jerseys', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (12, N'Long-Sleeve Logo Jersey, L', N'Clothing-Jerseys', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (13, N'Long-Sleeve Logo Jersey, XL', N'Clothing-Jerseys', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (14, N'HL Road Frame - Red, 62', N'Components-Road Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (15, N'HL Road Frame - Red, 44', N'Components-Road Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (16, N'HL Road Frame - Red, 48', N'Components-Road Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (17, N'HL Road Frame - Red, 52', N'Components-Road Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (18, N'HL Road Frame - Red, 56', N'Components-Road Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (19, N'LL Road Frame - Black, 58', N'Components-Road Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (20, N'LL Road Frame - Black, 60', N'Components-Road Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (21, N'LL Road Frame - Black, 62', N'Components-Road Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (22, N'LL Road Frame - Red, 44', N'Components-Road Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (23, N'LL Road Frame - Red, 48', N'Components-Road Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (24, N'LL Road Frame - Red, 52', N'Components-Road Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (25, N'LL Road Frame - Red, 58', N'Components-Road Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (26, N'LL Road Frame - Red, 60', N'Components-Road Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (27, N'LL Road Frame - Red, 62', N'Components-Road Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (28, N'ML Road Frame - Red, 44', N'Components-Road Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (29, N'ML Road Frame - Red, 48', N'Components-Road Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (30, N'ML Road Frame - Red, 52', N'Components-Road Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (31, N'ML Road Frame - Red, 58', N'Components-Road Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (32, N'ML Road Frame - Red, 60', N'Components-Road Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (33, N'LL Road Frame - Black, 44', N'Components-Road Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (34, N'LL Road Frame - Black, 48', N'Components-Road Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (35, N'LL Road Frame - Black, 52', N'Components-Road Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (36, N'HL Mountain Frame - Silver, 42', N'Components-Mountain Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (37, N'HL Mountain Frame - Silver, 44', N'Components-Mountain Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (38, N'HL Mountain Frame - Silver, 48', N'Components-Mountain Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (39, N'HL Mountain Frame - Silver, 46', N'Components-Mountain Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (40, N'HL Mountain Frame - Black, 42', N'Components-Mountain Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (41, N'HL Mountain Frame - Black, 44', N'Components-Mountain Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (42, N'HL Mountain Frame - Black, 48', N'Components-Mountain Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (43, N'HL Mountain Frame - Black, 46', N'Components-Mountain Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (44, N'HL Mountain Frame - Black, 38', N'Components-Mountain Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (45, N'HL Mountain Frame - Silver, 38', N'Components-Mountain Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (46, N'Road-150 Red, 62', N'Bikes-Road Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (47, N'Road-150 Red, 44', N'Bikes-Road Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (48, N'Road-150 Red, 48', N'Bikes-Road Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (49, N'Road-150 Red, 52', N'Bikes-Road Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (50, N'Road-150 Red, 56', N'Bikes-Road Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (51, N'Road-450 Red, 58', N'Bikes-Road Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (52, N'Road-450 Red, 60', N'Bikes-Road Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (53, N'Road-450 Red, 44', N'Bikes-Road Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (54, N'Road-450 Red, 48', N'Bikes-Road Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (55, N'Road-450 Red, 52', N'Bikes-Road Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (56, N'Road-650 Red, 58', N'Bikes-Road Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (57, N'Road-650 Red, 60', N'Bikes-Road Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (58, N'Road-650 Red, 62', N'Bikes-Road Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (59, N'Road-650 Red, 44', N'Bikes-Road Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (60, N'Road-650 Red, 48', N'Bikes-Road Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (61, N'Road-650 Red, 52', N'Bikes-Road Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (62, N'Road-650 Black, 58', N'Bikes-Road Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (63, N'Road-650 Black, 60', N'Bikes-Road Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (64, N'Road-650 Black, 62', N'Bikes-Road Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (65, N'Road-650 Black, 44', N'Bikes-Road Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (66, N'Road-650 Black, 48', N'Bikes-Road Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (67, N'Road-650 Black, 52', N'Bikes-Road Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (68, N'Mountain-100 Silver, 38', N'Bikes-Mountain Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (69, N'Mountain-100 Silver, 42', N'Bikes-Mountain Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (70, N'Mountain-100 Silver, 44', N'Bikes-Mountain Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (71, N'Mountain-100 Silver, 48', N'Bikes-Mountain Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (72, N'Mountain-100 Black, 38', N'Bikes-Mountain Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (73, N'Mountain-100 Black, 42', N'Bikes-Mountain Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (74, N'Mountain-100 Black, 44', N'Bikes-Mountain Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (75, N'Mountain-100 Black, 48', N'Bikes-Mountain Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (76, N'Mountain-200 Silver, 38', N'Bikes-Mountain Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (77, N'Mountain-200 Silver, 42', N'Bikes-Mountain Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (78, N'Mountain-200 Silver, 46', N'Bikes-Mountain Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (79, N'Mountain-200 Black, 38', N'Bikes-Mountain Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (80, N'Mountain-200 Black, 42', N'Bikes-Mountain Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (81, N'Mountain-200 Black, 46', N'Bikes-Mountain Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (82, N'Mountain-300 Black, 38', N'Bikes-Mountain Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (83, N'Mountain-300 Black, 40', N'Bikes-Mountain Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (84, N'Mountain-300 Black, 44', N'Bikes-Mountain Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (85, N'Mountain-300 Black, 48', N'Bikes-Mountain Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (86, N'Road-250 Red, 44', N'Bikes-Road Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (87, N'Road-250 Red, 48', N'Bikes-Road Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (88, N'Road-250 Red, 52', N'Bikes-Road Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (89, N'Road-250 Red, 58', N'Bikes-Road Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (90, N'Road-250 Black, 44', N'Bikes-Road Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (91, N'Road-250 Black, 48', N'Bikes-Road Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (92, N'Road-250 Black, 52', N'Bikes-Road Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (93, N'Road-250 Black, 58', N'Bikes-Road Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (94, N'Road-550-W Yellow, 38', N'Bikes-Road Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (95, N'Road-550-W Yellow, 40', N'Bikes-Road Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (96, N'Road-550-W Yellow, 42', N'Bikes-Road Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (97, N'Road-550-W Yellow, 44', N'Bikes-Road Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (98, N'Road-550-W Yellow, 48', N'Bikes-Road Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (99, N'LL Fork', N'Components-Forks', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (100, N'ML Fork', N'Components-Forks', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (101, N'HL Fork', N'Components-Forks', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (102, N'LL Headset', N'Components-Headsets', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (103, N'ML Headset', N'Components-Headsets', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (104, N'HL Headset', N'Components-Headsets', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (105, N'LL Mountain Handlebars', N'Components-Handlebars', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (106, N'ML Mountain Handlebars', N'Components-Handlebars', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (107, N'HL Mountain Handlebars', N'Components-Handlebars', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (108, N'LL Road Handlebars', N'Components-Handlebars', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (109, N'ML Road Handlebars', N'Components-Handlebars', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (110, N'HL Road Handlebars', N'Components-Handlebars', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (111, N'ML Mountain Frame - Black, 38', N'Components-Mountain Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (112, N'LL Mountain Front Wheel', N'Components-Wheels', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (113, N'ML Mountain Front Wheel', N'Components-Wheels', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (114, N'HL Mountain Front Wheel', N'Components-Wheels', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (115, N'LL Road Front Wheel', N'Components-Wheels', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (116, N'ML Road Front Wheel', N'Components-Wheels', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (117, N'HL Road Front Wheel', N'Components-Wheels', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (118, N'Touring Front Wheel', N'Components-Wheels', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (119, N'ML Road Frame-W - Yellow, 38', N'Components-Road Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (120, N'LL Mountain Rear Wheel', N'Components-Wheels', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (121, N'ML Mountain Rear Wheel', N'Components-Wheels', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (122, N'HL Mountain Rear Wheel', N'Components-Wheels', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (123, N'LL Road Rear Wheel', N'Components-Wheels', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (124, N'ML Road Rear Wheel', N'Components-Wheels', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (125, N'HL Road Rear Wheel', N'Components-Wheels', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (126, N'Touring Rear Wheel', N'Components-Wheels', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (127, N'ML Mountain Frame - Black, 40', N'Components-Mountain Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (128, N'ML Mountain Frame - Black, 44', N'Components-Mountain Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (129, N'ML Mountain Frame - Black, 48', N'Components-Mountain Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (130, N'ML Road Frame-W - Yellow, 40', N'Components-Road Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (131, N'ML Road Frame-W - Yellow, 42', N'Components-Road Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (132, N'ML Road Frame-W - Yellow, 44', N'Components-Road Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (133, N'ML Road Frame-W - Yellow, 48', N'Components-Road Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (134, N'HL Road Frame - Black, 62', N'Components-Road Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (135, N'HL Road Frame - Black, 44', N'Components-Road Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (136, N'HL Road Frame - Black, 48', N'Components-Road Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (137, N'HL Road Frame - Black, 52', N'Components-Road Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (138, N'Men''s Sports Shorts, S', N'Clothing-Shorts', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (139, N'Touring-Panniers, Large', N'Accessories-Panniers', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (140, N'Cable Lock', N'Accessories-Locks', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (141, N'Minipump', N'Accessories-Pumps', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (142, N'Mountain Pump', N'Accessories-Pumps', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (143, N'Taillights - Battery-Powered', N'Accessories-Lights', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (144, N'Headlights - Dual-Beam', N'Accessories-Lights', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (145, N'Headlights - Weatherproof', N'Accessories-Lights', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (146, N'Men''s Sports Shorts, M', N'Clothing-Shorts', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (147, N'Men''s Sports Shorts, L', N'Clothing-Shorts', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (148, N'Men''s Sports Shorts, XL', N'Clothing-Shorts', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (149, N'Women''s Tights, S', N'Clothing-Tights', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (150, N'Women''s Tights, M', N'Clothing-Tights', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (151, N'Women''s Tights, L', N'Clothing-Tights', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (152, N'Men''s Bib-Shorts, S', N'Clothing-Bib-Shorts', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (153, N'Men''s Bib-Shorts, M', N'Clothing-Bib-Shorts', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (154, N'Men''s Bib-Shorts, L', N'Clothing-Bib-Shorts', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (155, N'Half-Finger Gloves, S', N'Clothing-Gloves', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (156, N'Half-Finger Gloves, M', N'Clothing-Gloves', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (157, N'Half-Finger Gloves, L', N'Clothing-Gloves', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (158, N'Full-Finger Gloves, S', N'Clothing-Gloves', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (159, N'Full-Finger Gloves, M', N'Clothing-Gloves', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (160, N'Full-Finger Gloves, L', N'Clothing-Gloves', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (161, N'Classic Vest, S', N'Clothing-Vests', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (162, N'Classic Vest, M', N'Clothing-Vests', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (163, N'Classic Vest, L', N'Clothing-Vests', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (164, N'Women''s Mountain Shorts, S', N'Clothing-Shorts', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (165, N'Women''s Mountain Shorts, M', N'Clothing-Shorts', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (166, N'Women''s Mountain Shorts, L', N'Clothing-Shorts', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (167, N'Water Bottle - 30 oz.', N'Accessories-Bottles and Cages', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (168, N'Mountain Bottle Cage', N'Accessories-Bottles and Cages', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (169, N'Road Bottle Cage', N'Accessories-Bottles and Cages', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (170, N'Patch Kit/8 Patches', N'Accessories-Tires and Tubes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (171, N'Racing Socks, M', N'Clothing-Socks', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (172, N'Racing Socks, L', N'Clothing-Socks', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (173, N'Hitch Rack - 4-Bike', N'Accessories-Bike Racks', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (174, N'Bike Wash - Dissolver', N'Accessories-Cleaners', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (175, N'Fender Set - Mountain', N'Accessories-Fenders', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (176, N'All-Purpose Bike Stand', N'Accessories-Bike Stands', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (177, N'Hydration Pack - 70 oz.', N'Accessories-Hydration Packs', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (178, N'Short-Sleeve Classic Jersey, S', N'Clothing-Jerseys', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (179, N'Short-Sleeve Classic Jersey, M', N'Clothing-Jerseys', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (180, N'Short-Sleeve Classic Jersey, L', N'Clothing-Jerseys', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (181, N'Short-Sleeve Classic Jersey, XL', N'Clothing-Jerseys', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (182, N'HL Touring Frame - Yellow, 60', N'Components-Touring Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (183, N'LL Touring Frame - Yellow, 62', N'Components-Touring Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (184, N'HL Touring Frame - Yellow, 46', N'Components-Touring Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (185, N'HL Touring Frame - Yellow, 50', N'Components-Touring Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (186, N'HL Touring Frame - Yellow, 54', N'Components-Touring Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (187, N'HL Touring Frame - Blue, 46', N'Components-Touring Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (188, N'HL Touring Frame - Blue, 50', N'Components-Touring Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (189, N'HL Touring Frame - Blue, 54', N'Components-Touring Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (190, N'HL Touring Frame - Blue, 60', N'Components-Touring Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (191, N'Rear Derailleur', N'Components-Derailleurs', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (192, N'LL Touring Frame - Blue, 50', N'Components-Touring Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (193, N'LL Touring Frame - Blue, 54', N'Components-Touring Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (194, N'LL Touring Frame - Blue, 58', N'Components-Touring Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (195, N'LL Touring Frame - Blue, 62', N'Components-Touring Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (196, N'LL Touring Frame - Yellow, 44', N'Components-Touring Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (197, N'LL Touring Frame - Yellow, 50', N'Components-Touring Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (198, N'LL Touring Frame - Yellow, 54', N'Components-Touring Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (199, N'LL Touring Frame - Yellow, 58', N'Components-Touring Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (200, N'LL Touring Frame - Blue, 44', N'Components-Touring Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (201, N'ML Mountain Frame-W - Silver, 40', N'Components-Mountain Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (202, N'ML Mountain Frame-W - Silver, 42', N'Components-Mountain Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (203, N'ML Mountain Frame-W - Silver, 46', N'Components-Mountain Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (204, N'Rear Brakes', N'Components-Brakes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (205, N'LL Mountain Seat/Saddle', N'Components-Saddles', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (206, N'ML Mountain Seat/Saddle', N'Components-Saddles', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (207, N'HL Mountain Seat/Saddle', N'Components-Saddles', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (208, N'LL Road Seat/Saddle', N'Components-Saddles', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (209, N'ML Road Seat/Saddle', N'Components-Saddles', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (210, N'HL Road Seat/Saddle', N'Components-Saddles', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (211, N'LL Touring Seat/Saddle', N'Components-Saddles', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (212, N'ML Touring Seat/Saddle', N'Components-Saddles', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (213, N'HL Touring Seat/Saddle', N'Components-Saddles', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (214, N'LL Mountain Frame - Silver, 42', N'Components-Mountain Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (215, N'LL Mountain Frame - Silver, 44', N'Components-Mountain Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (216, N'LL Mountain Frame - Silver, 48', N'Components-Mountain Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (217, N'LL Mountain Frame - Silver, 52', N'Components-Mountain Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (218, N'Mountain Tire Tube', N'Accessories-Tires and Tubes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (219, N'Road Tire Tube', N'Accessories-Tires and Tubes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (220, N'Touring Tire Tube', N'Accessories-Tires and Tubes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (221, N'LL Mountain Frame - Black, 42', N'Components-Mountain Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (222, N'LL Mountain Frame - Black, 44', N'Components-Mountain Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (223, N'LL Mountain Frame - Black, 48', N'Components-Mountain Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (224, N'LL Mountain Frame - Black, 52', N'Components-Mountain Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (225, N'LL Mountain Tire', N'Accessories-Tires and Tubes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (226, N'ML Mountain Tire', N'Accessories-Tires and Tubes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (227, N'HL Mountain Tire', N'Accessories-Tires and Tubes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (228, N'LL Road Tire', N'Accessories-Tires and Tubes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (229, N'ML Road Tire', N'Accessories-Tires and Tubes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (230, N'HL Road Tire', N'Accessories-Tires and Tubes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (231, N'Touring Tire', N'Accessories-Tires and Tubes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (232, N'LL Mountain Pedal', N'Components-Pedals', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (233, N'ML Mountain Pedal', N'Components-Pedals', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (234, N'HL Mountain Pedal', N'Components-Pedals', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (235, N'LL Road Pedal', N'Components-Pedals', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (236, N'ML Road Pedal', N'Components-Pedals', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (237, N'HL Road Pedal', N'Components-Pedals', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (238, N'Touring Pedal', N'Components-Pedals', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (239, N'ML Mountain Frame-W - Silver, 38', N'Components-Mountain Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (240, N'LL Mountain Frame - Black, 40', N'Components-Mountain Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (241, N'LL Mountain Frame - Silver, 40', N'Components-Mountain Frames', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (242, N'Front Derailleur', N'Components-Derailleurs', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (243, N'LL Touring Handlebars', N'Components-Handlebars', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (244, N'HL Touring Handlebars', N'Components-Handlebars', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (245, N'Front Brakes', N'Components-Brakes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (246, N'LL Crankset', N'Components-Cranksets', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (247, N'ML Crankset', N'Components-Cranksets', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (248, N'HL Crankset', N'Components-Cranksets', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (249, N'Chain', N'Components-Chains', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (250, N'Touring-2000 Blue, 60', N'Bikes-Touring Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (251, N'Touring-1000 Yellow, 46', N'Bikes-Touring Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (252, N'Touring-1000 Yellow, 50', N'Bikes-Touring Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (253, N'Touring-1000 Yellow, 54', N'Bikes-Touring Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (254, N'Touring-1000 Yellow, 60', N'Bikes-Touring Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (255, N'Touring-3000 Blue, 54', N'Bikes-Touring Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (256, N'Touring-3000 Blue, 58', N'Bikes-Touring Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (257, N'Touring-3000 Blue, 62', N'Bikes-Touring Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (258, N'Touring-3000 Yellow, 44', N'Bikes-Touring Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (259, N'Touring-3000 Yellow, 50', N'Bikes-Touring Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (260, N'Touring-3000 Yellow, 54', N'Bikes-Touring Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (261, N'Touring-3000 Yellow, 58', N'Bikes-Touring Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (262, N'Touring-3000 Yellow, 62', N'Bikes-Touring Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (263, N'Touring-1000 Blue, 46', N'Bikes-Touring Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (264, N'Touring-1000 Blue, 50', N'Bikes-Touring Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (265, N'Touring-1000 Blue, 54', N'Bikes-Touring Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (266, N'Touring-1000 Blue, 60', N'Bikes-Touring Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (267, N'Touring-2000 Blue, 46', N'Bikes-Touring Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (268, N'Touring-2000 Blue, 50', N'Bikes-Touring Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (269, N'Touring-2000 Blue, 54', N'Bikes-Touring Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (270, N'Road-350-W Yellow, 40', N'Bikes-Road Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (271, N'Road-350-W Yellow, 42', N'Bikes-Road Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (272, N'Road-350-W Yellow, 44', N'Bikes-Road Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (273, N'Road-350-W Yellow, 48', N'Bikes-Road Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (274, N'Road-750 Black, 58', N'Bikes-Road Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (275, N'Touring-3000 Blue, 44', N'Bikes-Touring Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (276, N'Touring-3000 Blue, 50', N'Bikes-Touring Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (277, N'Mountain-400-W Silver, 38', N'Bikes-Mountain Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (278, N'Mountain-400-W Silver, 40', N'Bikes-Mountain Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (279, N'Mountain-400-W Silver, 42', N'Bikes-Mountain Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (280, N'Mountain-400-W Silver, 46', N'Bikes-Mountain Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (281, N'Mountain-500 Silver, 40', N'Bikes-Mountain Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (282, N'Mountain-500 Silver, 42', N'Bikes-Mountain Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (283, N'Mountain-500 Silver, 44', N'Bikes-Mountain Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (284, N'Mountain-500 Silver, 48', N'Bikes-Mountain Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (285, N'Mountain-500 Silver, 52', N'Bikes-Mountain Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (286, N'Mountain-500 Black, 40', N'Bikes-Mountain Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (287, N'Mountain-500 Black, 42', N'Bikes-Mountain Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (288, N'Mountain-500 Black, 44', N'Bikes-Mountain Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (289, N'Mountain-500 Black, 48', N'Bikes-Mountain Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (290, N'Mountain-500 Black, 52', N'Bikes-Mountain Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (291, N'LL Bottom Bracket', N'Components-Bottom Brackets', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (292, N'ML Bottom Bracket', N'Components-Bottom Brackets', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (293, N'HL Bottom Bracket', N'Components-Bottom Brackets', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (294, N'Road-750 Black, 44', N'Bikes-Road Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (295, N'Road-750 Black, 48', N'Bikes-Road Bikes', N'Available', 1000, N'')
GO
INSERT [dbo].[Product] ([Id], [Name], [Category], [Status], [Price], [PicturePath]) VALUES (296, N'Road-750 Black, 52', N'Bikes-Road Bikes', N'Available', 1000, N'')
GO
SET IDENTITY_INSERT [dbo].[Product] OFF
GO
