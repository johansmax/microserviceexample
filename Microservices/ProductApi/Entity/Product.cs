﻿using System.ComponentModel.DataAnnotations;

namespace ProductApi.Entity
{
    public class Product
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(50, ErrorMessage = "Max 50 chars")]
        public string Name { get; set; }
        [Required]

        [MaxLength(50, ErrorMessage = "Max 50 chars")]
        public string Category { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "Max 50 chars")]
        public string Status { get; set; }
        public int? Price { get; set; }

        [MaxLength(50, ErrorMessage = "Max 50 chars")]
        public string PicturePath { get; set; }

    }
}
