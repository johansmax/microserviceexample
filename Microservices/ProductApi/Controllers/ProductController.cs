﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProductApi.Entity;
using ProductApi.Infrastructure;

namespace ProductApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductRepository _productRepository;

        public ProductController(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<DTO.Product>>> GetProducts(string requestId, string name)
        {
            var list= await _productRepository.GetAvailableByProductName(name);
            var list2 = list.Select(x => new DTO.Product
            {
                Id = x.Id,
                Name = x.Name,
                Category = x.Category,
                Price = x.Price,
                PicturePath = x.PicturePath
                
            }).ToList();

            return list2;
        }
    }
}
