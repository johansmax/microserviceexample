﻿using Microsoft.EntityFrameworkCore;
using ProductApi.Entity;

namespace ProductApi.Infrastructure;

public sealed class ProductRepository : Repository<Product>, IProductRepository 
{
    public ProductRepository(DataContext context) : base(context)
    {
    }

    public async Task<List<Product>> GetAvailableByProductName(string name)
    {
        return await _context.Product.Where(x => (x.Name.Contains(name) || x.Category.Contains(name)) &&
                                                 x.Status=="Available"
        
        ).ToListAsync();
    }
}

public interface IProductRepository: IRepository<Product>
{
    Task<List<Product>> GetAvailableByProductName(string name);
}   